#include "Ball.h"
using namespace godot;

void Ball::_register_methods()
{
	register_method("_init", &Ball::_init);
	register_method("_ready", &Ball::_ready);
	register_method("_process", &Ball::_process);
	register_property("velocity", &Ball::velocity, 2);
	register_property("dir", &Ball::dir, Vector3(1, 0, 0));
}
void Ball::_init()
{

}

void Ball::_ready()
{
	dir = Vector3(rand() % 100 - 50, rand() % 100 - 50, rand() % 100 - 50).normalized();
	velocity = rand() % 10 + 2;
}

void Ball::_process(float delta)
{
	Ref<KinematicCollision> results = move_and_collide(dir * velocity * delta);
	if(results.is_valid())
	{
		Vector3 normal = results->get_normal();
		dir = dir - 2 * normal * (normal.dot(dir));
	}
}
