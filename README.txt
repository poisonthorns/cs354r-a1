Platform = Windows			

Project Description:
It took me forever to learn how to use Godot, so I only did the absolute base of this assignment. In Ball.cpp, on a collision
I just calculate what the ball needs to do based on the formula provided on the assignment page. I did make one change though.
I attached a camera to one of the three balls, so you are following one of the balls. I started with the camera attached to 
the ball in the youtube video then quickly changed it to be in the top corner of the room: https://youtu.be/99CY73yM24A

I used this video to learn how to set up the project and Godot: https://www.youtube.com/watch?v=XPcSfXsoArQ&feature=youtu.be
as well as the demo that was linked on the assignment page.

There's only really one file that has any meaningful changes. Ball.cpp in NativeLib/Ball.cpp contains the formula provided on
the assignment page to determine what happens to the ball when it collides with something.